<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (string $locale, int $datetype, int $timetype, $timezone = null, int $calendar = null, string $pattern = '', array $options): ?\IntlDateFormatter{
	$result = datefmt_create(
		$locale,
		$datetype,
		$timetype,
		$timezone,
		$calendar,
		$pattern
	);
	if (!$result) {
		return null;
	}
	return $result;
};
