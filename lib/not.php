<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Opposite of parameter.
 *	{{#if (not a)}}
 */
return function (): bool {
	$usefulargs = func_get_args();
	array_pop($usefulargs);
	if (count($usefulargs) == 0) {
		return true;
	}
	for ($i = 0; $i < count($usefulargs); $i++) {
		if (boolval($usefulargs[$i]) === true) {
			return false;
		}
	}
	return true;
};
