<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?array $arg1): array{
	if (is_null($arg1)) {
		return [];
	}
	return array_filter($arg1);
};
