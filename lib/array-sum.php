<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Calculate the sum of values in an array.
 *	{{array-sum a}}
 */
return function (?array $arg1): float {
	if (is_null($arg1)) {
		return 0;
	}
	return array_sum($arg1);
};
