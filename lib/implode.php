<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Joins array elements together.
 *	{{implode "," array}}
 *	{{implode "," (explode "a,b,c")}}
 */
return function (string $glue, ?array $pieces): string {
	return implode($glue, (array) $pieces);
};
