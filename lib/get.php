<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Get array property.
 *	{{get a "data.id"}}
 */
return function ($arg1, ?string $arg2) {
	if (empty($arg2)) {
		return null;
	}
	$depth = explode('.', $arg2);
	if (count($depth) == 1) {
		return $arg1[$depth[0]];
	} elseif (count($depth) == 2) {
		return $arg1[$depth[0]][$depth[1]];
	} elseif (count($depth) == 3) {
		return $arg1[$depth[0]][$depth[1]][$depth[2]];
	}
	return null;
};
