<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (string $url, string $component = 'PHP_URL_HOST', array $options): ?string {
	return parse_url($url, constant($component));
};
