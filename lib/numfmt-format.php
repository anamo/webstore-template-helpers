<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (NumberFormatter $fmt, $value, string $type, array $options): ?string {
	return numfmt_format($fmt, $value, constant('NumberFormatter::'.$type)) ?: null;
};
