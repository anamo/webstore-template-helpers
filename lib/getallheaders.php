<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (): ?array{
	$my_array = array_filter($_SERVER, fn($k) => 'HTTP_' == mb_substr($k, 0, 5), ARRAY_FILTER_USE_KEY);
	return array_combine(
		array_map(fn($v) => mb_substr($v, 5), array_keys($my_array)),
		array_values($my_array)
	);
};