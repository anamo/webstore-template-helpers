<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?array $keys, ?array $values, array $options): array{
	return array_combine((array) $keys, (array) $values);
};
