<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Opposite of parameter.
 *	{{#if (not a)}}
 */
/*
{{#each (sort-by (invoke (invoke product "hasMany" "variants") "asArray") "order")}}
{{#each (sort-by (invoke (invoke . "hasMany" "images") "asArray") "order")}}
<img src="{{invoke . 'getAttr' 'thumb'}}@480">
{{/each}}
{{/each}}
 */
return function (?\Market\OlympianNodes\OlympianNodeProduct $arg1): array{
	if (empty($arg1)) {
		return [];
	}
	$hasMany = $arg1->hasMany('variants');
	if ($hasMany->count() == 0) {
		return [];
	}
	$hasMany = $hasMany->asArray();
	usort($hasMany, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
	$hasMany = array_map(function ($v) {
		$hasMany1 = $v->hasMany('images');
		if ($hasMany1->count() == 0) {
			return [];
		}
		$hasMany1 = $hasMany1->asArray();
		usort($hasMany1, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
		return $hasMany1;
	}, $hasMany);
	return array_reduce($hasMany, 'array_merge', []);
};