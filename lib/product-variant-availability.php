<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?\Market\OlympianNodes\OlympianNodeProductVariant $arg1) {
	return $arg1->getAttr('track') ? $arg1->getAttr('stock') : $arg1->getAttr('availability');
};
