<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Left value is greater than or equal to right value.
 *	{{#if (gte a b)}}
 */
return function ($arg1, $arg2, array $options): bool {
	$left = $arg1;
	$right = $arg2;
	if (array_key_exists('forceNumber', $options['hash'])) {
		if (!is_numeric($left)) {
			$left = (float) $left;
		}
		if (!is_numeric($right)) {
			$right = (float) $right;
		}
	}
	return $left >= $right;
};
