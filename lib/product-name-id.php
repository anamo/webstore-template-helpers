<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?\Market\OlympianNodes\OlympianNodeProduct $arg1, ?string $arg2): string {
	if (empty($arg1)) {
		return '';
	}
	$hasMany0 = $arg1->hasMany('variants');
	if ($hasMany0->count() == 0) {
		return '';
	}
	$hasMany0 = $hasMany0->asArray();
	usort($hasMany0, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
	return reset(array_filter(array_map(function ($arg1) use (&$arg2) {
		// -------------------->>>>>>>>>>>> HELPERCOPY START @product-variant-name
		$hasMany = $arg1->hasMany('titles');
		if ($hasMany->count() == 0) {
			return '';
		}
		$translaction = !empty($arg2) ? array_find_by($hasMany->asArray(), 'getAttr', $arg2, 'locale') : null;
		if (!empty($translaction)) {
			return $translaction->getId();
		}
		if (empty(MANIFEST['locales'])) {
			return reset($hasMany->asArray())->getId();

		} else {
			if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
				return reset($hasMany->asArray())->getId();

			} else {
				$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('locale'), MANIFEST['locales'])));
				return !empty($translaction) ? $translaction->getId() : '';
			}
		}
		// -------------------->>>>>>>>>>>> HELPERCOPY END @product-variant-name
	}, $hasMany0)));
};