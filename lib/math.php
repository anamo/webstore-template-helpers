<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Do math.
 *	{{a "+" b}}
 */
return function ($arg1, $arg2, $arg3): float {
	$left = (float) $arg1;
	$right = (float) $arg3;
	switch ($arg2) {
		case '+':
			return $left + $right;
		case '-':
			return $left - $right;
		case '*':
			return $left * $right;
		case '/':
			return $left / $right;
		case '%':
			return $left % $right;
		default:
			return 0;
	}
};
