<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function ($arg1, int $arg2, array $options): float {
	return round($arg1, $arg2);
};
