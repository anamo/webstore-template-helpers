<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $arg1, array $options): bool {
	$comp = (true === $options['hash']['stripTags'] ? strip_tags(__($arg1)) : __($arg1));
	return mb_strlen($comp) > 0 && $comp !== $arg1;
};
