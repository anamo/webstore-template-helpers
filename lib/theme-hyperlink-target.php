<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**
 * Translate a string.
 *
 * {{theme-hyperlink-target "in~_self:about:blank"}}
 * "about:blank"
 *
 */
return function (?string $arg1, array $options): string {
	// parse url
	$parts = parse_url($arg1 = trim($arg1));
	$protocol_parts = explode('+', $parts['scheme']);
	$target = end(array_filter($protocol_parts, fn($v) => in_array($v, ['self', 'blank', 'parent'])));
	return $target ? "_$target" : "_self";
};
