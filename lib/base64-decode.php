<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $arg1, array $options) {
	$strict = false;
	if (isset($options['hash']['strict'])) {
		$strict = (bool) $options['hash']['strict'];
	}
	return base64_decode($arg1, $strict);
};
