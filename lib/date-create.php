<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (string $arg1 = 'now', \DateTimeZone $arg2 = null, array $options): ?\DateTime{
	$result = date_create($arg1, $arg2);
	if (!$result) {
		return null;
	}
	return $result;
};
