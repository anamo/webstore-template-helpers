<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?\Market\OlympianNodes\OlympianNodeProduct $arg1, ?string $arg2, array $options): ?\Market\OlympianNodes\OlympianNodeProductPrice {
	if (empty($arg1)) {
		return null;
	}
	$hasMany0 = $arg1->hasMany('variants');
	if ($hasMany0->count() == 0) {
		return null;
	}
	$hasMany0 = $hasMany0->asArray();
	usort($hasMany0, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
	$anyprice = array_filter(array_map(function ($arg1) use (&$arg2) {
		// -------------------->>>>>>>>>>>> HELPERCOPY START @product-variant-price
		$hasMany = $arg1->hasMany('prices');
		if ($hasMany->count() == 0) {
			return null;
		}
		$translaction = !empty($arg2) ? array_find_by($hasMany->asArray(), 'getAttr', locale_get_region($arg2), 'territory') : null;
		if (!empty($translaction)) {
			return $translaction;
		}
		$translaction = array_find_by($hasMany->asArray(), 'getAttr', '001', 'territory');
		if (!empty($translaction)) {
			return $translaction;
		}
		if (empty(MANIFEST['locales'])) {
			return reset($hasMany->asArray());

		} else {
			if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
				return null;

			} else {
				$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('territory'), array_map(fn($v) => locale_get_region(locale_canonicalize($v)), MANIFEST['locales'])))) ?? reset($hasMany->asArray());
				return $translaction;
			}
		}
		// -------------------->>>>>>>>>>>> HELPERCOPY END @product-variant-price
	}, $hasMany0));
	return empty($anyprice) ? null : reset($anyprice);
};