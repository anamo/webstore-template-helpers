<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Return first categoryname in current context or first one if none found.
 *	If site is multi-lang it shows the one that has not translation in anyone.
 *	{{category-seo . "en_IE"}}
 */
/*{{#if (find-by (invoke . "hasMany" "i18ns") "locale" "el_GR")}}
{{invoke (find-by (invoke . "hasMany" "i18ns") "locale" "el_GR") "getAttr" "seo"}}
{{else}}
{{invoke (object-at (invoke (invoke . "hasMany" "i18ns") 0) "asArray") "getAttr" "seo"}}
{{/if}}*/
return function (?\Market\OlympianNodes\OlympianNodeCategory $arg1, ?string $arg2): ?string {
	if (empty($arg1)) {
		return '';
	}
	$hasMany = $arg1->hasMany('i18ns');
	if ($hasMany->count() == 0) {
		return '';
	}
	$translaction = !empty($arg2) ? array_find_by($hasMany->asArray(), 'getAttr', $arg2, 'locale') : null;
	if (!empty($translaction)) {
		return $translaction->getAttr('seo-title');
	}
	if (empty(MANIFEST['locales'])) {
		return reset($hasMany->asArray())->getAttr('seo-title');

	} else {
		if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
			return reset($hasMany->asArray())->getAttr('seo-title');

		} else {
			$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('locale'), MANIFEST['locales'])));
			return !empty($translaction) ? $translaction->getAttr('seo-title') : '';
		}
	}
};