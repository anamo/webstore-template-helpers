<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Returns a new object copied from hash.
 *	{{context context=. product=.. blabla=a blbblb=b}}
 */
return function ($options): array{
	return $options['hash'];
};
