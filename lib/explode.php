<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Splits a string to an array.
 *	{{explode "," "a,b,c"}}
 *	{{implode "," (explode "a,b,c")}}
 */
return function (string $delimiter, ?string $string): array{
	return explode($delimiter, $string);
};
