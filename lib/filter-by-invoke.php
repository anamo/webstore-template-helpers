<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Returns the first item with a property matching the passed value.
 *	{{find-by-invoke a "function1" "156"}}
 *	{{find-by a "getAttr" "qty"}}
 */
return function ($arg1, string $arg2, array $arg3, $arg4, array $options): array{
	if (empty($arg1)) {
		return [];
	}
	$source = $arg1 instanceof \Market\OlympianNodes\Collection ? $arg1->asArray() : (array) $arg1;
	return array_filter($source, fn($v) => call_user_func([$v, $arg2], ...$arg3) == $arg4);
};
