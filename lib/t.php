<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**
 * Translate a string.
 *
 * {{t "apple"}}
 * "Mazá"
 *
 * {{t "Hello %s" "κόσμε"}}
 * "Γεια σου κόσμε"
 *
 * {{t "Hello %name%" name="κόσμε"}}
 * "Γεια σου κόσμε"
 *
 */
return function (?string $arg1, ...$rest): string {
	$options = array_pop($rest);
	if (empty($options['hash'])) {
		return __(...array_slice(func_get_args(), 0, -1));
	} else {
		return __($arg1, array_combine(array_map(fn($v) => "%$v%", array_keys($options['hash'])), $options['hash']));
	}
};