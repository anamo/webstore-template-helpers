<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**
 * Translate a string.
 *
 * {{theme-hyperlink-href "in~_self:about:blank"}}
 * "about:blank"
 *
 */
return function (?string $arg1, array $options): string {
	// parse url
	$parts = parse_url($arg1 = trim($arg1));
	$protocol_parts = explode('+', $parts['scheme']);
	$diff_parts = [];
	if (!empty($target = end(array_filter($protocol_parts, fn($v) => in_array($v, ['self', 'blank', 'parent']))))) {
		$protocol_parts = array_diff($protocol_parts, [$target]);
	}
	if (!empty(array_filter($protocol_parts, fn($v) => in_array($v, ['anid', 'bnid'])))) { // variant, product, category
		if (in_array('anid', $protocol_parts)) {
			$query_params = ['anid' => $parts['host']];
			if (!empty($parts['path'])) {
				$query_params['variant'] = trim($parts['path'], '/');
			}
			$parts['path'] = 'product';
			$parts['query'] = http_build_query($query_params);
			unset($parts['host']);
		} else {
			$parts['path'] = 'browse';
			$parts['query'] = http_build_query(['category' => $parts['host']]);
			unset($parts['host']);
		}
		array_walk($protocol_parts, function (&$v) {
			if (in_array($v, ['anid', 'bnid'])) {
				$v = 'https';
			}
		});
	}
	$parts['scheme'] = implode('+', $protocol_parts);
	if (empty($parts['host'])) {
		$parts['host'] = Webstore::getModel()->getAttr('domain');
		$parts['path'] = "/{$parts['path']}";
		if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
			$parts['path'] .= $_SERVER['APP_PATH_SUFFIX'];
		}
	}
	// build url
	$href = (isset($parts['scheme']) ? "{$parts['scheme']}:" : '').
		((isset($parts['user']) || isset($parts['host'])) ? '//' : '').
		(isset($parts['user']) ? "{$parts['user']}" : '').
		(isset($parts['pass']) ? ":{$parts['pass']}" : '').
		(isset($parts['user']) ? '@' : '').
		(isset($parts['host']) ? "{$parts['host']}" : '').
		(isset($parts['port']) ? ":{$parts['port']}" : '').
		(isset($parts['path']) ? "{$parts['path']}" : '').
		(isset($parts['query']) ? "?{$parts['query']}" : '').
		(isset($parts['fragment']) ? "#{$parts['fragment']}" : '');
	return $href;
};
