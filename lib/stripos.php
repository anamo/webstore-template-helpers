<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Find the position of the first occurrence of a case-insensitive substring in a string
 *	{{stripos "The quick brown fox jumps over the lazy dog." "fox"}}
 */
return function (string $haystack, string $needle) {
	return mb_stripos($haystack, $needle);
};
