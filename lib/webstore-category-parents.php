<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Return first categoryname in current context or first one if none found.
 *	If site is multi-lang it shows the one that has not translation in anyone.
 *	{{category-name . "en_IE"}}
 */
return function ($arg1): ?array{
	//$hasMany = $arg1->hasMany('categories');
	//$nodeId = end($hasMany->asArray())->getRel('category')['data']['id'];
	if (empty($node = array_find_by(Webstore::getCategories()->asArray(), 'getId', $arg1))) {
		return null;
	}
	$singlePath = array_filter(Webstore::getCategories()->asArray(), fn($v) => $v->getAttr('left') < $node->getAttr('left') &&
		$node->getAttr('left') < $v->getAttr('right'));
	usort($singlePath, fn($a, $b) => $a->getAttr('left') <=> $b->getAttr('right'));
	return $singlePath;
};