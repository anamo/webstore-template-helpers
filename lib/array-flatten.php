<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?array $arg1): array{
	$flat = [];
	array_walk_recursive($arg1, function (&$value) use (&$flat) {
		$flat[] = $value;
	});
	return $flat;
};
