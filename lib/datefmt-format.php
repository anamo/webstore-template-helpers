<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (\IntlDateFormatter $fmt, $value, array $options): ?string{
	$result = datefmt_format($fmt, $value);
	if (!$result) {
		return null;
	}
	return $result;
};
