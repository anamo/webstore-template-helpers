<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Get array element at index.
 *	{{object-at a 0}}
 *	{{object-at (invoke a "asArray") 0}}
 */
return function ($arg1, int $arg2) {
	if (empty($arg1)) {
		return null;
	}
	if ($arg1 instanceof \Market\OlympianNodes\Collection) {
		return $arg1->offsetGet($arg2);
	} else {
		return end(array_slice($arg1, $arg2, 1, true));
	}
};
