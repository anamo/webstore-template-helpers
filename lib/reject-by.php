<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Returns an array with just the items with the matched property.
 *	{{reject-by a "prop1" "156"}}
 */
return function ($arg1, string $arg2, $arg3): array{
	if (empty($arg1)) {
		return [];
	}
	$source = $arg1 instanceof \Market\OlympianNodes\Collection ? $arg1->asArray() : (array) $arg1;
	return array_filter($source, fn($v) => ($v instanceof \Market\OlympianNodes\OlympianNode ? $v->getAttr($arg2) : $v[$arg2]) != $arg3);
};