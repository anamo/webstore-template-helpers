<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function () {
	return 'ltr'; //TODO: CLDR https://stackoverflow.com/questions/47118326/how-to-get-direction-of-a-language-by-its-local-code-rtl-or-ltr
};
