<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (): string {
	$enc_iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('AES-256-OFB'));
	$crypted_token = openssl_encrypt(
		session_id(),
		'AES-256-OFB',
		openssl_digest(MANIFEST['access_key_secret'], 'sha256'),
		0,
		$enc_iv
	).'.'.bin2hex($enc_iv);
	return base64_encode($crypted_token); //hash_hmac('sha256', session_id(), MANIFEST['access_key_secret'], true));
};
