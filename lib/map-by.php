<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Returns the value of the named property on all items in the enumeration.
 *	{{map-by a "prop1" "123"}}
 */
return function ($arg1, string $arg2): array{
	if (empty($arg1)) {
		return [];
	}
	if ($arg1 instanceof \Market\OlympianNodes\Collection) {
		return $arg1->map(fn($v) => $v instanceof \Market\OlympianNodes\OlympianNode ? $v->getAttr($arg2) : $v[$arg2])->asArray();
	} else {
		return array_map(fn($v) => $v instanceof \Market\OlympianNodes\OlympianNode ? $v->getAttr($arg2) : $v[$arg2], $arg1);
	}
};