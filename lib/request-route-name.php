<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function () {
	$route = mb_strtolower(mb_substr(trim($_SERVER['APP_SCRIPT_NAME']), 1, -4));
	$route = preg_replace('/[\/]+/', '.', $route);
	$route = preg_replace('/[^\w\.]+/', '-', $route);
	$route = preg_replace('/\.+/', '.', $route);
	$route = preg_replace('/-+/', '-', $route);
	return $route;
};
