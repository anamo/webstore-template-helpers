<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**
 *	This is used for getting the PAGES settings of the current deployment.
 */
return function (): ?array{
	return PAGES;
};
