<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Checks if the given key or index exists in the array.
 *	{{array-key-exists "needle" heystack}}
 */
return function ($arg1, $arg2): bool {
	if (is_null($arg1) ||
		is_null($arg2) ||
		!is_array($arg2)) {
		return false;
	}
	return array_key_exists($arg1, $arg2);
};
