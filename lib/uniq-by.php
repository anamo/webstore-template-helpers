<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Returns a new enumerable that contains only items containing a unique property value.
 *	{{uniq-by a "prop1"}}
 */
return function ($arg1, string $arg2): array{
	if (empty($arg1)) {
		return [];
	}
	$source = $arg1 instanceof \Market\OlympianNodes\Collection ? $arg1->asArray() : (array) $arg1;
	return array_unique(array_map(fn($v) => $v instanceof \Market\OlympianNodes\OlympianNode ? $v->getAttr($arg2) : $v[$arg2], $source));
};