<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (): string {
	return array_key_exists('APP_PATH_SUFFIX', $_SERVER) ? $_SERVER['APP_PATH_SUFFIX'] : '';
};
