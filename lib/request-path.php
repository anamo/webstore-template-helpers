<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (): string {
	return $_SERVER['APP_SELF'].(array_key_exists('APP_PATH_SUFFIX', $_SERVER) ? (mb_substr('/' == $_SERVER['APP_SELF'], -1) ? mb_substr($_SERVER['APP_PATH_SUFFIX'], 1) : $_SERVER['APP_PATH_SUFFIX']) : '');
};
