<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Merge arrays to one array.
 *	{{array-merge a b c}}
 */
return function (): array{
	$usefulargs = func_get_args();
	array_pop($usefulargs);
	return array_merge(...$usefulargs);
};
