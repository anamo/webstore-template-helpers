<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**
 * Returns locale without the keywords.
 * http://userguide.icu-project.org/locale
 */
return function (string $locale, array $options): string {
	$localized_locale = locale_canonicalize($locale);
	return trim(reset(explode('@', $localized_locale, 2)));
};
