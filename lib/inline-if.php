<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function () {
	$args = func_get_args();
	$conditional = array_shift($args);
	$truepart = array_shift($args);
	if (1 < count($args)) {
		$falsepart = array_shift($args);
	}
	$options = array_shift($args);
	if ($conditional) {
		return $truepart;
	} else {
		return $falsepart;
	}
};
