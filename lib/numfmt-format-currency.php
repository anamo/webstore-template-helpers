<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (NumberFormatter $fmt, float $value, ?string $currency, array $options): ?string {
	if (is_null($currency)) {
		return null;
	}
	return numfmt_format_currency($fmt, $value, $currency) ?: null;
};
