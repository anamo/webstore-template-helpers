<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

//http://ogp.me/
//https://developers.facebook.com/docs/reference/opengraph/object-type/product.item/
//https://developers.facebook.com/docs/reference/opengraph/object-type/product/
//https://developers.google.com/search/docs/guides/intro-structured-data

return function (?\Market\OlympianNodes\OlympianNodeProduct $arg1): string {
	if (empty($arg1)) {
		return json_encode([]);
	}
	$resp = [
		'@context' => 'https://olympian.anamo.one',
		'@type' => 'product',
		'anid' => $arg1->getId(),
		'ordered_variants' => []
	];
	// variants
	$hasManyVariants = $arg1->hasMany('variants')->asArray() ?? [];
	usort($hasManyVariants, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
	array_walk($hasManyVariants, function ($v) use (&$resp) {
		$resp2 = [
			'@type' => 'variant',
			'id' => $v->getId(),
			'default' => $v->getAttr('order') == 1,
			'ordered_images' => []
		];
		$hasManyImages = $v->hasMany('images')->asArray() ?? [];
		usort($hasManyImages, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
		foreach ($hasManyImages as $v2) {
			$resp2['ordered_images'][] = [
				'@type' => 'image',
				'id' => $v2->getId(),
				'thumb' => $v2->getAttr('thumb')
			];
		}
		$resp['ordered_variants'][] = $resp2;
	});
	return json_encode($resp);
};
