<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $arg1, array $options) {
	$assoc = false;
	if (isset($options['hash']['assoc'])) {
		$assoc = (bool) $options['hash']['assoc'];
	}
	$depth = 512;
	if ($options['hash']['depth'] > 0) {
		$depth = (int) $options['hash']['depth'];
	}
	$opts = 0;
	if ($options['hash']['options'] > 0) {
		$opts = (int) $options['hash']['options'];
	}
	return json_decode($arg1, $assoc, $depth, $opts);
};
