<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**
 *  This is used for getting the Collection of all the current of the current Webstore.
 *  This is reloaded on every page load.
 *
 * {{#with (api "/categories?ipp=100&include=i18ns")}} {{/with}}
 * Shorthand for {{#with (webstore-categories)}} {{/with}}
 */
return function (): ?\Market\OlympianNodes\OlympianNodeCheckoutZone {
	return Webstore::getCheckoutZone();
};
