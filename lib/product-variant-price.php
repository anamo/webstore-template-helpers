<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?\Market\OlympianNodes\OlympianNodeProductVariant $arg1, ?string $locale): ?\Market\OlympianNodes\OlympianNodeProductPrice {
	if (empty($arg1)) {
		return null;
	}
	$hasMany = $arg1->hasMany('prices');
	if ($hasMany->count() == 0) {
		return null;
	}
	$translaction = !empty($locale) ? array_find_by($hasMany->asArray(), 'getAttr', locale_get_region($locale), 'territory') : null;
	if (!empty($translaction)) {
		return $translaction;
	}
	$translaction = array_find_by($hasMany->asArray(), 'getAttr', '001', 'territory');
	if (!empty($translaction)) {
		return $translaction;
	}
	if (empty(MANIFEST['locales'])) {
		return reset($hasMany->asArray());

	} else {
		if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
			return null;

		} else {
			$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('territory'), array_map(fn($v) => locale_get_region(locale_canonicalize($v)), MANIFEST['locales'])))) ?? reset($hasMany->asArray());
			return $translaction;
		}
	}
};