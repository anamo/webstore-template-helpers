<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	All parameters are equal.
 *	{{#if (empty a)}}
 */
return function ($arg1): bool {
	return empty($arg1);
};
