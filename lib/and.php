<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	All parameters are true.
 *	{{#if (and a b)}}
 */
return function () {
	$usefulargs = func_get_args();
	array_pop($usefulargs);
	if (count($usefulargs) == 0) {
		return false;
	}
	for ($i = 0; $i < count($usefulargs); $i++) {
		if (boolval($usefulargs[$i]) === false) {
			return $usefulargs[$i];
		}
	}
	return $usefulargs[count($usefulargs) - 1];
};
