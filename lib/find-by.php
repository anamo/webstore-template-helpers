<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Returns the first item with a property matching the passed value.
 *	{{find-by a "prop1" "156"}}
 */
return function ($arg1, string $arg2, $arg3) {
	if (empty($arg1)) {
		return null;
	}
	$source = $arg1 instanceof \Market\OlympianNodes\Collection ? $arg1->asArray() : (array) $arg1;
	return reset(array_filter($source, fn($v) => ($v instanceof \Market\OlympianNodes\OlympianNode ? $v->getAttr($arg2) : $v[$arg2]) == $arg3));
};