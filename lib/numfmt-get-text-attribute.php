<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (NumberFormatter $fmt, string $attr, array $options): ?string {
	return numfmt_get_text_attribute($fmt, constant('NumberFormatter::'.$attr)) ?: null;
};
