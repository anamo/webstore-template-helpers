<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?\Market\OlympianNodes\OlympianNodeProduct $arg1): array{
	if (empty($arg1)) {
		return [];
	}
	$hasMany = $arg1->hasMany('variants');
	if ($hasMany->count() == 0) {
		return [];
	}
	$groups = ['color', 'clothes-size', 'object-length', 'object-height', 'object-width', 'object-parts'];
	$hasMany = array_filter($hasMany->asArray(), function ($variant) use (&$groups) {
		$hasMany1 = $variant->hasMany('options');
		if ($hasMany1->count() == 0) {
			return false;
		}
		return !empty(array_diff(array_map(fn($v) => $v->getAttr('type'), $hasMany1->asArray()), $groups)) ||
		!empty(array_filter(array_count_values(array_intersect(array_map(fn($v) => $v->getAttr('type'), $hasMany1->asArray()), $groups)), fn($v) => $v > 1));
	});
	usort($hasMany, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
	return $hasMany;
};
