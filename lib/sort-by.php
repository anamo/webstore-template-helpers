<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Returns a new enumerable that contains only items containing a unique property value.
 *	{{sort-by a "prop1"}}
 */
return function ($arg1, string $arg2): array{
	if (empty($arg1)) {
		return [];
	}
	$source = array_values($arg1 instanceof \Market\OlympianNodes\Collection ? $arg1->asArray() : (array) $arg1);
	usort($source, fn($a, $b) => ($a instanceof \Market\OlympianNodes\OlympianNode ? $a->getAttr($arg2) : $a[$arg2]) <=> ($b instanceof \Market\OlympianNodes\OlympianNode ? $b->getAttr($arg2) : $b[$arg2]));
	return $source;
};