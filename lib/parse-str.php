<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $query_str, array $options): ?array{
	parse_str(trim($query_str), $query_params);
	return (array) $query_params;
};
