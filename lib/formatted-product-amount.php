<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?\Market\OlympianNodes\OlympianNodeProductVariant $arg1, ?string $locale, array $options): string {
	$fmt1 = numfmt_create($locale, NumberFormatter::CURRENCY);
	$defaultCurrencyCode1 = numfmt_get_text_attribute($fmt1, NumberFormatter::CURRENCY_CODE) ?? 'EUR';
	if (array_key_exists('decimals', $options['hash'])) {
		$fmt1->setAttribute(NumberFormatter::FRACTION_DIGITS, (int) $options['hash']['decimals']);
	}
	if (empty($arg1)) {
		return numfmt_format_currency($fmt1, 0, $defaultCurrencyCode1);
	}
	$hasMany = $arg1->hasMany('prices');
	if ($hasMany->count() == 0) {
		return numfmt_format_currency($fmt1, 0, $defaultCurrencyCode1);
	}
	$translaction = !empty($locale) ? array_find_by($hasMany->asArray(), 'getAttr', locale_get_region($locale), 'territory') : null;
	if (!empty($translaction)) {
		return numfmt_format_currency($fmt1, $translaction->getAttr('amount'), $defaultCurrencyCode1);
	}
	$translaction = array_find_by($hasMany->asArray(), 'getAttr', '001', 'territory');
	if (!empty($translaction)) {
		return numfmt_format_currency($fmt1, $translaction->getAttr('amount'), $defaultCurrencyCode1);
	}
	if (empty(MANIFEST['locales'])) {
		return numfmt_format_currency($fmt1, reset($hasMany->asArray())->getAttr('amount'), $defaultCurrencyCode1);

	} else {
		if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
			return numfmt_format_currency($fmt1, 0, $defaultCurrencyCode1);

		} else {
			$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('territory'), array_map(fn($v) => locale_get_region(locale_canonicalize($v)), MANIFEST['locales'])))) ?? reset($hasMany->asArray());
			$fmt2 = numfmt_create(locale_canonicalize('-'.$translaction->getAttr('territory')), NumberFormatter::CURRENCY);
			$defaultCurrencyCode2 = numfmt_get_text_attribute($fmt2, NumberFormatter::CURRENCY_CODE) ?? 'EUR';
			if (array_key_exists('decimals', $options['hash'])) {
				$fmt2->setAttribute(NumberFormatter::FRACTION_DIGITS, (int) $options['hash']['decimals']);
			}
			return numfmt_format_currency($fmt2, $translaction->getAttr('amount'), $defaultCurrencyCode2);
		}
	}
};