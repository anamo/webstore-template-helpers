<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Invoke object function.
 *	{{invoke a "getAttr" "order"}}
 */
return function ($arg, string $ee, $ee2) {
	if (!method_exists($arg, $ee)) {
		return null;
	}
	return $arg->{$ee}($ee2);
};
