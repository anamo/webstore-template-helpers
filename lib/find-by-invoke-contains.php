<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Returns an array with just the items with the matched property.
 *	{{find-by-invoke-contains a "function1" (explode "," "1,5,6")}}
 *	{{find-by-invoke-contains a "getId" (explode "," "1,5,6")}}
 */
return function ($arg1, string $arg2, array $arg3, $arg4, array $options) {
	if (empty($arg1)) {
		return null;
	}
	$source = $arg1 instanceof \Market\OlympianNodes\Collection ? $arg1->asArray() : (array) $arg1;
	return reset(array_filter($source, fn($v) => in_array(call_user_func([$v, $arg2], ...$arg3), $arg4)));
};