<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Normalizes the input provided and returns the normalized string
 *	{{normalize "αυτό"}}
 *	{{normalize "αυτό" 3}}
 */
return function (?string $arg1, int $arg2 = 4): string {
	if (is_null($arg1)) {
		return '';
	}
	return normalizer_normalize($arg1, $arg2);
};
