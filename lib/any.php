<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Return first not empty parameter.
 *	{{any a b}}
 *	{{#with (any a b)}}
 */
return function () {
	$usefulargs = func_get_args();
	array_pop($usefulargs);
	if (count($usefulargs) == 0) {
		return null;
	}
	for ($i = 0; $i < count($usefulargs); $i++) {
		if (!empty($usefulargs[$i])) {
			return $usefulargs[$i];
		}
	}
	return null;
};
