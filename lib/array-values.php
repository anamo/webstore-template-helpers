<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Return all the values of an array. Keys are reset to 0..1..2..3...n
 *	{{array-values a}}
 */
return function (?array $arg1): array{
	if (is_null($arg1)) {
		return [];
	}
	return array_values($arg1);
};
