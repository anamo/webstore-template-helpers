<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $arg1, array $options): string {
	$opts = 0;
	if ($options['hash']['options'] > 0) {
		$opts = (int) $options['hash']['options'];
	}
	return trim(json_encode($arg1, $opts), '"');
};
