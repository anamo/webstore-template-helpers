<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**
 *	This is used for getting the OlympianWebstore of the current Webstore.
 *	This is reloaded on every page load.
 */
return function (): ?\Market\OlympianNodes\OlympianNodeWebstore {
	return Webstore::getModel();
};
