<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (int $dividend, int $divisor): int {
	return intdiv($dividend, $divisor);
};
