<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?array $arg1, string $arg2, $filter = null): array{ // TODO: Add $arg3 with locale of webstore (checkout-zone or default and do similar with product-variant-name)
	if (empty($arg1)) {
		return [];
	}
	usort($arg1, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
	$ert = function ($arr, $type) {
		$resp = [];
		foreach ($arr as $v) {
			$hasMany1 = $v->hasMany('options');
			if ($hasMany1->count() == 0) {
				continue;
			}
			$hasMany1 = $hasMany1->asArray();
			usort($hasMany1, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
			$hasMany1 = array_filter($hasMany1, fn($v) => $v->getAttr('type') == $type);
			foreach ($hasMany1 as $v2) {
				$hasMany2 = $v2->hasMany('i18ns');
				if ($hasMany2->count() == 0) {
					continue;
				}
				$hasMany2 = $hasMany2->asArray();
				foreach ($hasMany2 as $v3) {
					$resp[$v3->getAttr('text')][] = $v->getId();
				}
			}
		}
		return $resp;
	};
	$hasMany = $ert($arg1, $arg2);

	if ('available' === $filter) {
		$hasMany3 = [];
		$arg1 = array_filter($arg1, fn($v) => $v->getAttr('track') ? 0 < $v->getAttr('stock') : in_array($v->getAttr('availability'), ["in", "in23", "in12"]));
		$hasMany3 = $ert($arg1, $arg2);
		$order = array_keys($hasMany);
		uksort($hasMany3, fn($key1, $key2) => array_search($key1, $order) <=> array_search($key2, $order));
		return $hasMany3;
	}

	return $hasMany;
};
