<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**
 * Find the most apropriate product-variant-title for the supplied variant and locale.
 * If the variant is Empty or there are no titles, return empty string.
 * If there is a title that matches the locale return it.
 * Else, try find one that best matches current situation.
 */
return function (?\Market\OlympianNodes\OlympianNodeProductVariant $arg1, ?string $arg2): string {
	if (empty($arg1)) {
		return '';
	}
	$hasMany = $arg1->hasMany('descriptions');
	if ($hasMany->count() == 0) {
		return '';
	}
	$translaction = !empty($arg2) ? array_find_by($hasMany->asArray(), 'getAttr', $arg2, 'locale') : null;
	if (!empty($translaction)) {
		return $translaction->getAttr('i18n');
	}
	if (empty(MANIFEST['locales'])) {
		return reset($hasMany->asArray())->getAttr('i18n');

	} else {
		if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
			return reset($hasMany->asArray())->getAttr('i18n');

		} else {
			$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('locale'), MANIFEST['locales'])));
			return !empty($translaction) ? $translaction->getAttr('i18n') : '';
		}
	}
};