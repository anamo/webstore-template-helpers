<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $arg1) {
	if (is_null($arg1)) {
		return $_POST;
	}
	return !empty($arg1) ? $_POST[$arg1] : $_POST;
};
