<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	All parameters are identical(type checking too).
 *	{{#if (identical a b)}}
 */
return function ($arg1, $arg2): bool {
	return $arg1 === $arg2;
};
