<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $arg1, ?array $arg2, ?array $arg3): string {
	$newArgs = (array) $arg2;
	foreach ((array) $arg3 as $k => $v) {
		if (is_array($v)) {
			[$arrKey, $arrVal] = $v;
			if (is_array($newArgs[$k])) {
				unset($newArgs[$k][$arrKey]);
			} else {
				unset($newArgs[$k]);
			}
		}
		if (is_array($v)) {
			$newArgs[$k.'['.$arrKey.']'] = $arrVal;
		} else {
			$newArgs[$k] = $v;
		}
	}
	$newArgs = array_filter($newArgs);
	return trim($arg1).(!empty($newArgs) ? '?' : '').http_build_query($newArgs, '', ini_get('arg_separator.output'), PHP_QUERY_RFC3986);
};
