<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (string $name, string $value = '', array $options): ?bool {
	return setcookie($name, $value, $options['hash']['expires'] ?? '', $options['hash']['path'] ?? '', $options['hash']['domain'] ?? '', $options['hash']['secure'] ?? '', $options['hash']['httponly'] ?? '');
};
