<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Generate a unique ID.
 *	{{uniqid "" true}}
 */
return function (string $prefix = '', bool $more_entropy = false): string {
	return uniqid($prefix, $more_entropy);
};
