<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Return an array with all the product variants.
 *	{{product-variants a}}
 */
return function (?\Market\OlympianNodes\OlympianNodeProduct $arg1): array{
	if (empty($arg1)) {
		return [];
	}
	$hasMany = $arg1->hasMany('variants');
	if ($hasMany->count() == 0) {
		return [];
	}
	$hasMany = $hasMany->asArray();
	usort($hasMany, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
	return $hasMany;
};