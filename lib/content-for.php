<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $path): string {
	return 'pages'.DIRECTORY_SEPARATOR.stream_resolve_include_path(locale_canonicalize(locale_get_default())).DIRECTORY_SEPARATOR.$path.'.hbs'; // NOTE: On windows locales don't parse the same as with linux. So we can't use setlocale(LC_ALL, 0);
};
