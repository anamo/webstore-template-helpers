<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (string $locale, string $style, array $options): ?NumberFormatter {
	if ('en_001' == mb_substr(strtolower(trim($locale)), 0, 6)) {
		$localized_locale = locale_canonicalize($locale);
		$keywords_part = trim(explode('@', $localized_locale, 2)[1]);
		$locale = 'en_US'.(!empty($keywords_part) ? "@$keywords_part" : '');
	}
	$fmt = numfmt_create($locale, constant("NumberFormatter::$style"));
	if (!$fmt) {
		return null;
	}
	foreach ($options['hash'] as $k => $v) {
		$fmt->setAttribute(constant("NumberFormatter::$k"), $v);
	}
	return $fmt;
};
