<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Transliterates a string
 *	{{normalize "NFD; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove;" "αυΤό"}}
 *	{{normalize "NFD; [:Nonspacing Mark:] Remove; NFC; [:Punctuation:] Remove; Lower();" "αυτό"}}
 */
return function (?string $transliterator, ?string $subject): string {
	return transliterator_transliterate($transliterator, $subject);
};
