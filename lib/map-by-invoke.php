<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Returns the value of the named property on all items in the enumeration.
 *	{{map-by-invoke a "function1" "123"}}
 *	{{map-by-invoke a "getAttr" "qty"}}
 */
return function ($arg1, string $arg2, $arg3): array{
	if (empty($arg1)) {
		return [];
	}
	$source = $arg1 instanceof \Market\OlympianNodes\Collection ? $arg1->asArray() : (array) $arg1;
	return array_map(fn($v) => call_user_func([$v, $arg2], $arg3), $source);
};