<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Return first categoryname in current context or first one if none found.
 *	If site is multi-lang it shows the one that has not translation in anyone.
 *	{{category-name . "en_IE"}}
 */
return function (?\Market\OlympianNodes\OlympianNode $arg1, ?\Market\OlympianNodes\Collection $arg2): array{
	if (is_null($arg1) ||
		is_null($arg2)) {
		return [];
	}
	return array_filter($arg2->asArray(), fn($v) => $v->getAttr('left') > $arg1->getAttr('left') &&
		$v->getAttr('right') < $arg1->getAttr('right') &&
		$v->getAttr('depth') == $arg1->getAttr('depth') + 1);
};