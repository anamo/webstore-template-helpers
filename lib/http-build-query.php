<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (array $options): ?string {
	if (empty($options)) {
		return null;
	}
	return http_build_query($options['hash'], '', ini_get('arg_separator.output'), PHP_QUERY_RFC3986);
};
