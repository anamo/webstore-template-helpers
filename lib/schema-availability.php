<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $arg1): string {
	$schema_availabilities_map = [
		'in' => 'https://schema.org/InStock',
		'out' => 'https://schema.org/OutOfStock',
		'sign' => 'https://schema.org/PreSale',
		'un' => 'https://schema.org/SoldOut',
		'in23' => 'https://schema.org/InStock',
		'in12' => 'https://schema.org/PreOrder'
	];

	if (empty($arg1)) {
		return $schema_availabilities_map['out'];
	}

	if (!array_key_exists($arg1, $schema_availabilities_map)) {
		return $schema_availabilities_map['out'];
	}

	return $schema_availabilities_map[$arg1];
};
