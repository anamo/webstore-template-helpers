<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $arg1, ?array $arg2, string $arg3, $arg4): string {
	$newArgs = (array) $arg2;
	$isArray = stripos($arg3, '[');
	if (false !== $isArray) {
		[$arrKey, $arrVal] = explode('[', rtrim($arg3, ']'));
		if (is_array($newArgs[$arrKey])) {
			unset($newArgs[$arrKey][$arrVal]);
		} else {
			unset($newArgs[$arrKey]);
		}
	}
	if (!empty($arg3)) {
		$newArgs[$arg3] = $arg4;
	}
	$newArgs = array_filter($newArgs);
	return trim($arg1).(!empty($newArgs) ? '?' : '').http_build_query($newArgs, '', ini_get('arg_separator.output'), PHP_QUERY_RFC3986);
};
