<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $str, array $options): int {
	if (is_null($str)) {
		return 0;
	}
	return mb_strlen($str);
};
