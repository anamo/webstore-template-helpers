<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

//http://ogp.me/
//https://developers.facebook.com/docs/reference/opengraph/object-type/product.item/
//https://developers.facebook.com/docs/reference/opengraph/object-type/product/
//https://developers.google.com/search/docs/guides/intro-structured-data

return function (?\Market\OlympianNodes\OlympianNodeProduct $arg1): string {
	if (empty($arg1)) {
		return json_encode([]);
	}
	$resp = [
		'@context' => 'http://schema.org/',
		'@type' => 'Product'
	];
	// variants
	$hasManyVariants = $arg1->hasMany('variants')->asArray() ?? [];
	usort($hasManyVariants, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));

	$exemplar_variant = reset($hasManyVariants);

	$localized_locale = locale_canonicalize(trim(locale_get_default()));
	$basename_locale = trim(reset(explode('@', $localized_locale, 2)));

	// -------------------->>>>>>>>>>>> HELPERCOPY START @product-name
	if (!empty($name = reset(array_filter(array_map(function (?\Market\OlympianNodes\OlympianNodeProductVariant $arg1) use (&$basename_locale) {
		$hasMany = $arg1->hasMany('titles');
		if ($hasMany->count() == 0) {
			return '';
		}
		$translaction = !empty($basename_locale) ? array_find_by($hasMany->asArray(), 'getAttr', $basename_locale, 'locale') : null;
		if (!empty($translaction)) {
			return $translaction->getAttr('i18n');
		}
		if (empty(MANIFEST['locales'])) {
			return reset($hasMany->asArray())->getAttr('i18n');

		} else {
			if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
				return reset($hasMany->asArray())->getAttr('i18n');

			} else {
				$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('locale'), MANIFEST['locales'])));
				return !empty($translaction) ? $translaction->getAttr('i18n') : '';
			}
		}
	}, $hasManyVariants))))) {
		$resp['name'] = $name;
	}
	// -------------------->>>>>>>>>>>> HELPERCOPY END @product-name

	// -------------------->>>>>>>>>>>> HELPERCOPY START @product-description
	if (!empty($description = reset(array_filter(array_map(function (?\Market\OlympianNodes\OlympianNodeProductVariant $arg1) use (&$basename_locale) {
		$hasMany = $arg1->hasMany('descriptions');
		if ($hasMany->count() == 0) {
			return '';
		}
		$translaction = !empty($basename_locale) ? array_find_by($hasMany->asArray(), 'getAttr', $basename_locale, 'locale') : null;
		if (!empty($translaction)) {
			return $translaction->getAttr('i18n');
		}
		if (empty(MANIFEST['locales'])) {
			return reset($hasMany->asArray())->getAttr('i18n');

		} else {
			if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
				return reset($hasMany->asArray())->getAttr('i18n');

			} else {
				$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('locale'), MANIFEST['locales'])));
				return !empty($translaction) ? $translaction->getAttr('i18n') : '';
			}
		}
	}, $hasManyVariants))))) {
		$tags = ['</p>', '<br />', '<br/>', '<br>', '<hr />', '<hr/>', '<hr>', '</h1>', '</h2>', '</h3>', '</h4>', '</h5>', '</h6>'];
		$non_html_description = str_replace($tags, "\n\n", trim($description));
		$resp['description'] = strip_tags($non_html_description);
	}
	// -------------------->>>>>>>>>>>> HELPERCOPY END @product-description

	// -------------------->>>>>>>>>>>> HELPERCOPY START @product-images
	if (!empty($image = reset(array_reduce(array_filter(array_map(function (?\Market\OlympianNodes\OlympianNodeProductVariant $arg1) {
		$hasMany1 = $arg1->hasMany('images');
		if ($hasMany1->count() == 0) {
			return [];
		}
		$hasMany1 = $hasMany1->asArray();
		usort($hasMany1, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
		return array_map(fn($item) => $item->getSource('512'), $hasMany1);
	}, $hasManyVariants)), 'array_merge', [])))) {
		$resp['image'] = $image;
	}
	// -------------------->>>>>>>>>>>> HELPERCOPY END @product-images

	$schema_availabilities_map = [
		'in' => 'https://schema.org/InStock',
		'out' => 'https://schema.org/OutOfStock',
		'sign' => 'https://schema.org/PreSale',
		'un' => 'https://schema.org/SoldOut',
		'in23' => 'https://schema.org/InStock',
		'in12' => 'https://schema.org/PreOrder'
	];

	if (!empty($anyprice = reset(array_filter(array_map(function (?\Market\OlympianNodes\OlympianNodeProductVariant $arg1) use (&$basename_locale, &$schema_availabilities_map) {
		// -------------------->>>>>>>>>>>> HELPERCOPY START @product-variant-price
		$hasMany = $arg1->hasMany('prices');
		if ($hasMany->count() == 0) {
			return null;
		}
		$translaction = !empty($basename_locale) ? array_find_by($hasMany->asArray(), 'getAttr', locale_get_region($basename_locale), 'territory') : null;
		if (!empty($translaction)) {
			return $translaction;
		}
		$translaction = array_find_by($hasMany->asArray(), 'getAttr', '001', 'territory');
		if (!empty($translaction)) {
			return $translaction;
		}
		if (empty(MANIFEST['locales'])) {
			return reset($hasMany->asArray());

		} else {
			if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
				return null;

			} else {
				$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('territory'), array_map(fn($v) => locale_get_region(locale_canonicalize($v)), MANIFEST['locales'])))) ?? reset($hasMany->asArray());
				return $translaction;
			}
		}
		// -------------------->>>>>>>>>>>> HELPERCOPY END @product-variant-price
	}, $hasManyVariants))))) {

		$market_availability = $exemplar_variant->getAttr('availability');

		if ($anyprice->getAttr('amount') < $anyprice->getAttr('compare')) {
			$resp['offers'] = [
				'@type' => 'AggregateOffer',
				'lowPrice' => $anyprice->getAttr('amount'),
				'highPrice' => $anyprice->getAttr('compare'),
				'priceCurrency' => numfmt_get_text_attribute(numfmt_create(trim(locale_get_default()), NumberFormatter::CURRENCY), NumberFormatter::CURRENCY_CODE),
				'availability' => $schema_availabilities_map[$market_availability]
			];
		} else {
			$resp['offers'] = [
				'@type' => 'Offer',
				'price' => $anyprice->getAttr('amount'),
				'priceCurrency' => numfmt_get_text_attribute(numfmt_create(trim(locale_get_default()), NumberFormatter::CURRENCY), NumberFormatter::CURRENCY_CODE),
				'availability' => $schema_availabilities_map[$market_availability]
			];
		}
	}

	// brand
	foreach ($arg1->hasMany('attributes') as $attr) {
		foreach (Webstore::getManufacturers() as $filter_item_node) {
			if ($attr->belongsTo('option')->getId() == $filter_item_node->getId()) {
				// -------------------->>>>>>>>>>>> HELPERCOPY START @webstore-filter-item-name
				if (!empty($brand = (function (?\Market\OlympianNodes\OlympianNodeFilterItem $arg1) use (&$basename_locale) {
					$hasMany = $arg1->hasMany('i18ns');
					if ($hasMany->count() == 0) {
						return '';
					}
					$translaction = !empty($basename_locale) ? array_find_by($hasMany->asArray(), 'getAttr', $basename_locale, 'locale') : null;
					if (!empty($translaction)) {
						return $translaction->getAttr('text');
					}
					if (empty(MANIFEST['locales'])) {
						return reset($hasMany->asArray())->getAttr('text');

					} else {
						if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
							return reset($hasMany->asArray())->getAttr('text');

						} else {
							$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('locale'), MANIFEST['locales'])));
							return !empty($translaction) ? $translaction->getAttr('text') : '';
						}
					}
				})($filter_item_node))) {
					$resp['brand'] = [
						'@type' => 'Brand',
						'name' => $brand
					];
				}
				// -------------------->>>>>>>>>>>> HELPERCOPY END @webstore-filter-item-name
			}
		}
	}

	// mpn
	if (!empty($mpn = reset($hasManyVariants)->getAttr('mpn'))) {
		$resp['mpn'] = $mpn;
	}

	// sku
	if (!empty($sku = reset($hasManyVariants)->getAttr('sku'))) {
		$resp['sku'] = $sku;
	}

	$hasMany = array_filter($hasManyVariants, function (?\Market\OlympianNodes\OlympianNodeProductVariant $variant_node) {
		$hasMany1 = $variant_node->hasMany('options');
		if ($hasMany1->count() == 0) {
			return false;
		}
		return empty(array_diff(array_map(fn($v) => $v->getAttr('type'), $hasMany1->asArray()), ['color', 'clothes-size'])) &&
		empty(array_filter(array_count_values(array_intersect(array_map(fn($v) => $v->getAttr('type'), $hasMany1->asArray()), ['color', 'clothes-size'])), fn($v) => $v > 1));
	});
	usort($hasMany, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));

	array_walk($hasMany, function ($variant_node) use (&$resp, &$basename_locale, &$anyprice, &$exemplar_variant) {
		$model = [
			'@type' => 'ProductModel'
		];

		// name
		$model['name'] = (function (?\Market\OlympianNodes\OlympianNodeProductVariant $arg1) use (&$basename_locale) {
			$hasMany = $arg1->hasMany('titles');
			if ($hasMany->count() == 0) {
				return '';
			}
			$translaction = !empty($basename_locale) ? array_find_by($hasMany->asArray(), 'getAttr', $basename_locale, 'locale') : null;
			if (!empty($translaction)) {
				return $translaction->getAttr('i18n');
			}
			if (empty(MANIFEST['locales'])) {
				return reset($hasMany->asArray())->getAttr('i18n');

			} else {
				if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
					return reset($hasMany->asArray())->getAttr('i18n');

				} else {
					$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('locale'), MANIFEST['locales'])));
					return !empty($translaction) ? $translaction->getAttr('i18n') : '';
				}
			}
		})($variant_node) ?: $resp['name'];

		// image
		$hasManyImages = $variant_node->hasMany('images')->asArray() ?? [];
		usort($hasManyImages, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
		if (!empty($hasManyImages)) {
			$model['image'] = array_filter(array_map(fn(?\Market\OlympianNodes\OlympianNodeProductImage $image_node) => $image_node->getSource('512'), $hasManyImages));
		}

		// price
		$anyprice2 = (function (?\Market\OlympianNodes\OlympianNodeProductVariant $arg1) use (&$basename_locale, &$schema_availabilities_map) {
			// -------------------->>>>>>>>>>>> HELPERCOPY START @product-variant-price
			$hasMany = $arg1->hasMany('prices');
			if ($hasMany->count() == 0) {
				return null;
			}
			$translaction = !empty($basename_locale) ? array_find_by($hasMany->asArray(), 'getAttr', locale_get_region($basename_locale), 'territory') : null;
			if (!empty($translaction)) {
				return $translaction;
			}
			$translaction = array_find_by($hasMany->asArray(), 'getAttr', '001', 'territory');
			if (!empty($translaction)) {
				return $translaction;
			}
			if (empty(MANIFEST['locales'])) {
				return reset($hasMany->asArray());

			} else {
				if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
					return null;

				} else {
					$translaction = reset(array_filter($hasMany->asArray(), fn($v) => !in_array($v->getAttr('territory'), array_map(fn($v) => locale_get_region(locale_canonicalize($v)), MANIFEST['locales'])))) ?? reset($hasMany->asArray());
					return $translaction;
				}
			}
			// -------------------->>>>>>>>>>>> HELPERCOPY END @product-variant-price
		})($variant_node) ?: $anyprice;

		if (!empty($anyprice2)) {

			$market_availability = $exemplar_variant->getAttr('availability');

			if ($anyprice2->getAttr('amount') < $anyprice2->getAttr('compare')) {
				$model['offers'] = [
					'@type' => 'AggregateOffer',
					'lowPrice' => $anyprice2->getAttr('amount'),
					'highPrice' => $anyprice2->getAttr('compare'),
					'priceCurrency' => numfmt_get_text_attribute(numfmt_create(trim(locale_get_default()), NumberFormatter::CURRENCY), NumberFormatter::CURRENCY_CODE),
					'availability' => $schema_availabilities_map[$market_availability]
				];
			} else {
				$model['offers'] = [
					'@type' => 'Offer',
					'price' => $anyprice2->getAttr('amount'),
					'priceCurrency' => numfmt_get_text_attribute(numfmt_create(trim(locale_get_default()), NumberFormatter::CURRENCY), NumberFormatter::CURRENCY_CODE),
					'availability' => $schema_availabilities_map[$market_availability]
				];
			}

		}

		// color
		$colors = array_filter($variant_node->hasMany('options')->asArray(), fn($item) => 'color' == $item->getAttr('type'));
		usort($colors, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
		if (!empty($color = reset(array_filter(array_map(function ($v2) {
			$hasMany2 = $v2->hasMany('i18ns');
			if ($hasMany2->count() == 0) {
				return null;
			}
			$hasMany2 = $hasMany2->asArray();
			return reset($hasMany2)->getAttr('text');
		}, $colors))))) {
			$model['color'] = $color;
		}

		// size
		$sizes = array_filter($variant_node->hasMany('options')->asArray(), fn($item) => 'clothes-size' == $item->getAttr('type'));
		usort($sizes, fn($a, $b) => $a->getAttr('order') <=> $b->getAttr('order'));
		if (!empty($size = reset(array_filter(array_map(function ($v2) {
			$hasMany2 = $v2->hasMany('i18ns');
			if ($hasMany2->count() == 0) {
				return null;
			}
			$hasMany2 = $hasMany2->asArray();
			return reset($hasMany2)->getAttr('text');
		}, $sizes))))) {
			$model['size'] = $size;
		}

		$resp['model'][] = $model;

	});
	return json_encode($resp, JSON_UNESCAPED_UNICODE);
};
