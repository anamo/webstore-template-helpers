<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Return first categoryname in current context or first one if none found.
 *	If site is multi-lang it shows the one that has not translation in anyone.
 *	{{category-name . "en_IE"}}
 */
return function (?\Market\OlympianNodes\OlympianNode $arg1, ?\Market\OlympianNodes\Collection $arg2): string {
	if (is_null($arg1) ||
		is_null($arg2)) {
		return [];
	}
	$children = array_filter($arg2->asArray(), fn($v) => $v->getAttr('left') > $arg1->getAttr('left') &&
		$v->getAttr('right') < $arg1->getAttr('right') &&
		$v->getAttr('depth') == $arg1->getAttr('depth') + 1);
	if (empty($children)) {
		return 'flat';
	}
	return empty(array_filter($children, fn($v) => !empty(array_filter($arg2->asArray(), fn($v2) => $v2->getAttr('left') > $v->getAttr('left') &&
		$v2->getAttr('right') < $v->getAttr('right') &&
		$v2->getAttr('depth') == $v->getAttr('depth') + 1)))) ? 'flat' : 'tree';
};