<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?\Market\OlympianNodes\OlympianNodeProduct $arg1, ?string $arg2): string {
	if (empty($arg1)) {
		return '#';
	}
	$hasMany = $arg1->hasMany('slugs');
	if ($hasMany->count() == 0) {
		return '/product'.$_SERVER['APP_PATH_SUFFIX'].'?anid='.$arg1->getId();
	}
	$hasManyWithSlugs = array_filter($hasMany->asArray(), fn($v) => !empty($v->getAttr('slug')));
	if (count($hasManyWithSlugs) == 0) {
		return '/product'.$_SERVER['APP_PATH_SUFFIX'].'?anid='.$arg1->getId();
	}
	$translaction = !empty($arg2) ? array_find_by($hasManyWithSlugs, 'getAttr', $arg2, 'locale') : null;
	if (!empty($translaction)) {
		return '/'.$translaction->getAttr('slug').$_SERVER['APP_PATH_SUFFIX'];
	}
	if (empty(MANIFEST['locales'])) {
		return '/'.reset($hasManyWithSlugs)->getAttr('slug').$_SERVER['APP_PATH_SUFFIX'];

	} else {
		if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
			return '/product'.$_SERVER['APP_PATH_SUFFIX'].'?anid='.$arg1->getId();

		} else {
			$translaction = reset(array_filter($hasManyWithSlugs, fn($v) => !in_array($v->getAttr('locale'), MANIFEST['locales'])));
			return !empty($translaction) ? '/'.$translaction->getAttr('slug').$_SERVER['APP_PATH_SUFFIX'] : '/product'.$_SERVER['APP_PATH_SUFFIX'].'?anid='.$arg1->getId();
		}
	}
};