<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function ($arg1, ?string $allowable_tags, array $options): string {
	if (empty($arg1)) {
		return '';
	}
	return strip_tags($arg1, trim($allowable_tags));
};
