<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (\DateTime $arg1, string $arg2, array $options): ?\DateTime{
	$result = date_modify($arg1, $arg2);
	if (!$result) {
		return null;
	}
	return $result;
};
