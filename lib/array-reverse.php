<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Merge arrays to one array.
 *	{{array-reverse a}}
 */
return function (?array $arg1, array $options): array{
	if (is_null($arg1)) {
		return [];
	}
	return array_reverse($arg1);
};
