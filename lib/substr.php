<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $str, int $start, ?int $length, array $options): string {
	return mb_substr($str, $start, $length);
};
