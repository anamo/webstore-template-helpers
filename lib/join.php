<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Joins array elements together.
 *	{{join array}}
 *	{{join "a" "b" "c" separator=","}}
 */
return function (): string {
	$args = func_get_args();
	$options = array_slice($args, -1);
	return implode($options['hash']['separator'] ?? '', array_slice($args, 0, -1));
};
