<?php /*! anamo/market-dummy-deployment v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/market-dummy-deployment */

return function (?string $path, array $options): string {
	if (empty($san_path = trim($path))) {
		return $san_path;
	}
	$type = 'dist/' == mb_substr($san_path, 0, 5) ? 'dist' : 'bundles';
	$hash = 'dist' == $type ? 'DIST_HASH' : 'BUNDLES_HASH';
	return (!empty(getenv('WS_DEP_ID')) &&
		!empty(getenv($hash)) ? 'https://mrt.azureedge.net/'.getenv('WS_DEP_ID').'/'.$type.'/'.getenv($hash) : '/'.$type).'/'.mb_substr($san_path, mb_strlen($type) + 1);
};
