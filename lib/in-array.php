<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Array contains value.
 *	{{#if (in-array a b)}}
 */
return function ($arg1, ?array $arg2): bool {
	if (empty($arg2)) {
		return false;
	}
	return in_array($arg1, $arg2);
};
