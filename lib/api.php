<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?string $query, array $options) {
	return Webstore::pageviewBatch($query);
};
