<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

return function (?\Market\OlympianNodes\OlympianNodeProduct $arg1) {
	if (1 == $arg1->hasMany('variants')->count()) {
		$variant = $arg1->hasMany('variants')->offsetGet(0);
		return $variant->getAttr('track') ? $variant->getAttr('stock') : $variant->getAttr('availability');
	} else {
		$in_stock = array_filter($arg1->hasMany('variants')->asArray(), fn($v) => $v->getAttr('track') ? 0 < $v->getAttr('stock') : in_array($v->getAttr('availability'), ["in", "in23", "in12"]));
		if (0 < count($in_stock)) {
			$track_in_stock = array_filter($arg1->hasMany('variants')->asArray(), fn($v) => $v->getAttr('track'));
			return $arg1->hasMany('variants')->count() == count($track_in_stock) ? array_sum(array_map(fn($v) => max(0, $v->getAttr('stock')), $track_in_stock)) : 'in';
		} else {
			return 'out';
		}
	}
};
