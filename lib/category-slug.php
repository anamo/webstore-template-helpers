<?php /*! anamo/webstore-template-helpers v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/webstore-template-helpers */

/**	Return first categoryname in current context or first one if none found.
 *	If site is multi-lang it shows the one that has not translation in anyone.
 *	{{category-name . "en_IE"}}
 */
/*IF NO LOCALES SET -> JUST LOOK UP URLS, SHOW FIRST SLUGIE PRD-TITLES AND CTG-NAMES
IF LOCALES SET
IF CURRENTLY IN A URL-LOCALE -> LOOK UP URLS OF THIS LOCALE, SHOW PRD-TITLES AND CTG-NAMES OF THIS LOCALE

IF CURRENTLY IN NO URL-LOCALE -> LOOK UP URLS NOT IN THE LOCALES, SHOW FIRST PRD-TITLES AND CTG-NAMES NOT IN THE LOCALES

(looking up is done in router.php)
(prd-titles and cat-names in helpers)*/
return function (?\Market\OlympianNodes\OlympianNodeCategory $arg1, ?string $arg2): string {
	if (empty($arg1)) {
		return '#';
	}
	$hasMany = $arg1->hasMany('i18ns');
	if ($hasMany->count() == 0) {
		return '/browse'.$_SERVER['APP_PATH_SUFFIX'].'?category='.$arg1->getId();
	}
	$hasManyWithSlugs = array_filter($hasMany->asArray(), fn($v) => !empty($v->getAttr('slug')));
	if (count($hasManyWithSlugs) == 0) {
		return '/browse'.$_SERVER['APP_PATH_SUFFIX'].'?category='.$arg1->getId();
	}
	$translaction = !empty($arg2) ? array_find_by($hasManyWithSlugs, 'getAttr', $arg2, 'locale') : null;
	if (!empty($translaction)) {
		return '/'.$translaction->getAttr('slug').$_SERVER['APP_PATH_SUFFIX'];
	}
	if (empty(MANIFEST['locales'])) {
		return '/'.reset($hasManyWithSlugs)->getAttr('slug').$_SERVER['APP_PATH_SUFFIX'];

	} else {
		if (array_key_exists('APP_PATH_SUFFIX', $_SERVER)) {
			return '/browse'.$_SERVER['APP_PATH_SUFFIX'].'?category='.$arg1->getId();

		} else {
			$translaction = reset(array_filter($hasManyWithSlugs, fn($v) => !in_array($v->getAttr('locale'), MANIFEST['locales'])));
			return !empty($translaction) ? '/'.$translaction->getAttr('slug').$_SERVER['APP_PATH_SUFFIX'] : '/browse'.$_SERVER['APP_PATH_SUFFIX'].'?category='.$arg1->getId();
		}
	}
};